#include <Ethernet.h>
#include <MQTT.h>
#include <DHT.h>
#include <Wire.h> // for BH1750 
#include <BH1750.h> 

//DHT
const int DHTPIN = 4;
const int DHTTYPE = DHT22;  //Type of DHT 22 or 11
DHT dht(DHTPIN, DHTTYPE);

//BH1750
BH1750 lightMeter;   

//water flow
volatile int flow_frequency; // Measures flow sensor pulses
unsigned int l_hour; // Calculated litres/hour
unsigned char flowsensor = 2; // Sensor Input
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
byte ip[] = {192, 168, 0, 12};  // <- change to match your network

EthernetClient net;
MQTTClient client;

void connect() {
    Serial.print("connecting...");
    while (!client.connect("client_name_0", "pi", "123456")) {
        Serial.print(".");
        delay(1000);
    }
    client.subscribe("control/switch-0/ras-to-ard", 2);
    Serial.println("\nconnected!");
}
                
void messageReceived(String &topic, String &payload) {
    Serial.println("incoming: " + topic + " - " + payload); 
   
    if (payload == "turn relay-0 on"){
        digitalWrite(9, LOW);
        client.publish("control/switch-0/ard-to-ras", "turn 0 relay-0 1", false, 2);
    }
    else if (payload == "turn relay-0 off"){
        digitalWrite(9, HIGH);
        client.publish("control/switch-0/ard-to-ras", "turn 0 relay-0 0", false, 2);
    }
    else if (payload == "turn relay-1 on"){
        digitalWrite(8, LOW);
        client.publish("control/switch-0/ard-to-ras", "turn 1 relay-1 1", false, 2);
    }   
    else if (payload == "turn relay-1 off"){
        digitalWrite(8, HIGH);
        client.publish("control/switch-0/ard-to-ras", "turn 1 relay-1 0", false, 2);
    }
    else if (payload == "turn relay-2 on"){
        digitalWrite(7, LOW);
        client.publish("control/switch-0/ard-to-ras", "turn 2 relay-2 1", false, 2);
    }   
    else if (payload == "turn relay-2 off"){
        digitalWrite(7, HIGH);
        client.publish("control/switch-0/ard-to-ras", "turn 2 relay-2 0", false, 2);
    }
    else if (payload == "turn relay-3 on"){
        digitalWrite(6, LOW);
        client.publish("control/switch-0/ard-to-ras", "turn 3 relay-3 1", false, 2);
    }   
    else if (payload == "turn relay-3 off"){
        digitalWrite(6, HIGH);
        client.publish("control/switch-0/ard-to-ras", "turn 3 relay-3 0", false, 2);
    }
}

void setup() {
    //water flow
    pinMode(flowsensor, INPUT);
    digitalWrite(flowsensor, HIGH); // Optional Internal Pull-Up
    attachInterrupt(0, flow, RISING); // Setup Interrupt
    sei(); // Enable interrupts

  
    pinMode(9, OUTPUT);
    digitalWrite(9, HIGH);
    pinMode(8, OUTPUT);
    digitalWrite(8, HIGH);
    pinMode(7, OUTPUT);
    digitalWrite(7, HIGH);
    pinMode(6, OUTPUT);
    digitalWrite(6, HIGH);
    Serial.begin(9600);
    Ethernet.begin(mac, ip);

    // Note: Local domain names (e.g. "Computer.local" on OSX) are not supported by Arduino.
    // You need to set the IP address directly.
    client.begin("192.168.0.4", net);
    client.onMessage(messageReceived);

    connect();
   
    dht.begin();  
    Wire.begin();
    lightMeter.begin();
    Serial.println("Setup OK");
}

//water flow
void flow () // Interrupt function
{
   flow_frequency++;
}


word period = 59000;
unsigned long time_now = 0;

void loop() {
    client.loop();
      
    if (!client.connected()) {
        Serial.println("Disconnected...");
        connect();
    } 
    if((unsigned long)(millis() - time_now) > period){
        time_now = millis();
        //DHT
        float h = dht.readHumidity();
        float t = dht.readTemperature();
        //BH1750
        uint16_t lux = lightMeter.readLightLevel();
        l_hour = (flow_frequency * 60 / 7.5); // (Pulse frequency x 60 min) / 7.5Q = flowrate in L/hour
        flow_frequency = 0; // Reset Counter

        // publish a message roughly every x second.
        Serial.println(String(t) + " " + String(h) + " " + String(lux) + " " + String(l_hour));   
        client.publish("sensors/arduino-0/ard-to-ras/now", String(t) + " " + String(h) + " " + String(lux) + " " + String(l_hour), false, 2);   
    }
}
